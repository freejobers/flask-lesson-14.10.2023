import flask
from flask import Flask
import numpy as np
from flask import render_template, request
import pickle
import sklearn
import tensorflow as tf

from sklearn.tree import DecisionTreeRegressor

app = Flask(__name__)


def isNumeric(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


@app.route('/', methods=['get', 'post'])
def index():
    message = ''
    if request.method == 'POST':
        IW = request.form.get('IW')
        IF = request.form.get('IF')
        VW = request.form.get('VW')
        FP = request.form.get('FP')

        if isNumeric(IW) and isNumeric(IF) and isNumeric(VW) and isNumeric(FP):
            param = [[float(IW), float(IF), float(VW), float(FP)]]

            dnn_model_loaded = tf.keras.models.load_model('dnn_model')

            # Загрузка параметров нормализации
            mean = np.load('scaler_mean.npy')
            std = np.load('scaler_std.npy')

            X_new_normalized = (param - mean) / std

            pred = dnn_model_loaded.predict(X_new_normalized)

            Depth = round(pred[0][0], 2)
            Weight = round(pred[0][1], 2)

            message = f'Глубина сварного шва: {Depth:.2f}, Ширина сварного шва: {Weight:.2f}'

    return render_template('index.html', message=message)


